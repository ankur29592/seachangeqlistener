#!/bin/bash
echo "seachangeMonitor.sh started at: $(date +%x-%r)"
restart_threshold=5
nio_restart_cnt=0
jms_restart_cnt=0
export MAILLIST="virendra.gulhane@alticeusa.com"
export HOST=$(hostname | awk -F. '{print $1}')
while true
do
#Checking NIO Listener Service
NIO_SERVICE='SeachangeListener'
echo "Checking service $NIO_SERVICE, Time stamp: $(date +%x-%r)"
nio_result=$(ps -ef | grep SeachangeListener | grep -v grep | awk '{print $2}')
if [ ${nio_result} > 0 ]; then
    echo "$NIO_SERVICE service running, OK"
    nio_restart_cnt=0
else
        nio_restart_cnt=$((nio_restart_cnt + 1))
        echo "$NIO_SERVICE is not running! Restarting service: $NIO_SERVICE, Restart count: $nio_restart_cnt"
        nohup ./startSeachangeNIOListener.sh >> /dev/null &
        if [ ${nio_restart_cnt} -gt $restart_threshold ]; then
              echo "Threshold value: ${restart_threshold}"
              echo "NIO_LISTENER restart reached threshold count! Sending mail now"
              echo "NIO_LISTENER service restart reached threshold value: $restart_threshold, HOST: $HOST, TIME: $(date +%x-%r)" | mail -s "NIO_LISTENER service restart reached threshold" $MAILLIST
              echo "Setting NIO restart count to zero"
              nio_restart_cnt=0
        fi
fi
#Checking JMS Q Service
JMS_Q_SERVICE='SeachangeInputReceiver'
echo "Checking service $JMS_Q_SERVICE, Time stamp: $(date +%x-%r)"
jms_result=$(ps -ef | grep SeachangeInputReceiver | grep -v grep | awk '{print $2}')
if [ ${jms_result} > 0 ]; then
    echo "$JMS_Q_SERVICE service running, OK"
    jms_restart_cnt=0
else
                jms_restart_cnt=$((jms_restart_cnt + 1))
                echo "$JMS_Q_SERVICE is NOT running! Restarting service: JMS_Q_LISTENER, Restart count: $jms_restart_cnt"
                nohup ./startSeachangeJMSListener.sh >> /dev/null &
                if [ ${jms_restart_cnt} -gt $restart_threshold ]; then
                    echo "Threshold value: ${restart_threshold}"
                    echo "JMS_Q_LISTENER restart reached threshold count! Sending mail now"
                    echo "JMS_Q_LISTENER service restart reached threshold value: $restart_threshold, HOST: $HOST, TIME: $(date +%x-%r)" | mail -s "JMS_Q_LISTENER service restart reached threshold" $MAILLIST
                    echo "Setting JMS restart count to zero"
                    jms_restart_cnt=0                     
                fi
fi
sleep 5m
done
echo "seachangeMonitor.sh finished at: $(date +%x-%r)"
