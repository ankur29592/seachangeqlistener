#!/bin/ksh
# This shell script starts SeachangeNIOListener app
BASEDIR=$(dirname "$0")
source $BASEDIR/../conf/seachangeNIOListener.properties
MyName=startSeachangeNIOListener
CLIENT_CLASSES=$BASEDIR/../app/SeachangeNIOListener.jar
CLIENT_LIB=$BASEDIR/../lib
CLIENT_CONF_FILES=$BASEDIR/../conf
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
cd $SCRIPTPATH
SC_LOG_DIR=${SCRIPTPATH}/../logs
#get current timestamp
current_time=$(date +%Y%m%d_%H%M%S)
#exporting paths
export SC_LOG_DIR_PATH=$SC_LOG_DIR
export SC_CUR_DATETIME=$current_time
echo "$MyName: INFO: Starting startSeachangeNIOListener Process ..."
##
## APPLICATION CLASSPATH
##
for i in  ${CLIENT_LIB}/*.jar ${CLIENT_CONF_FILES}/*.properties ${CLIENT_CONF_FILES}/*.xml; do
  if [ "X${APP_CLASSPATH}" != "X" ]; then
    APP_CLASSPATH=${APP_CLASSPATH}:$i
   else
    APP_CLASSPATH=$i
  fi
done
#echo "Classpath Before = "$APP_CLASSPATH
APP_CLASSPATH=${APP_CLASSPATH}:${JAVA_HOME}:${CLIENT_CLASSES}:${CLIENT_CONF_FILES}/
#echo "Classpath= "$APP_CLASSPATH

nohup $JAVA_HOME/bin/java -classpath ${APP_CLASSPATH} com.alticeusa.seachangelistener.server.SeachangeListener start