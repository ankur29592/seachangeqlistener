#!/bin/ksh
# This shell script  starts SeachangeJMSListener app
JAVA_HOME=/oolshr/jdk/jdk1.8.0_72
export JAVA_HOME
PATH=$JAVA_HOME/bin:$PATH
export PATH
MyName=startSeachangeJMSListener
CLIENT_CLASSES=../app/VODSeachange.jar
CLIENT_LIB=../lib
#CLIENT_CONF_FILES=../lib/*.*
##
## APPLICATION CLASSPATH
##
#APP_CLASSPATH=${JAVA_HOME}:${CLIENT_CLASSES}:"/oolshr/tomcat8/servers/nestClusterC/Seachange_Virendra/lib/:."
#APP_CLASSPATH=${JAVA_HOME}:${CLIENT_CLASSES}:${CLIENT_LIB}:${CLIENT_CONF_FILES}:

echo "$MyName: INFO: Starting Process ..."

for i in  ${CLIENT_LIB}/*.jar ${CLIENT_LIB}/*.properties ${CLIENT_LIB}/*.xml; do
  if [ "X${APP_CLASSPATH}" != "X" ]; then
    APP_CLASSPATH=${APP_CLASSPATH}:$i
   else
    APP_CLASSPATH=$i
  fi
done
#echo "Classpath Before = "$APP_CLASSPATH
APP_CLASSPATH=${APP_CLASSPATH}:${JAVA_HOME}:${CLIENT_CLASSES}:${CLIENT_LIB}/
#echo "Classpath= "$APP_CLASSPATH
#echo "ARGSPATH= "${ARGSPATH}
java -classpath ${APP_CLASSPATH} com.alticeusa.VODSeachange.queue.SeachangeInputReceiver
