#killall startSeachangeNIOListener.sh
PIDS=`ps -axf |grep startSeachangeNIOListener.sh|grep -v "grep"|grep -o '^[ ]*[0-9]*' `
JavaPIDS=`ps -axf |grep SeachangeListener |grep -v "grep"|grep -o '^[ ]*[0-9]*' `

################################### Kill shell script instance  ##############
if [ -z "$PIDS" ]; then
  echo "Process not running." 1>&2
  exit 1
else
  for PID in $PIDS; do
    echo " SProcessID killed :" $PID
    kill -9 $PID
  done
fi
################################ Kill Java instance #############
if [ -z "$JavaPIDS" ]; then
  echo "Process not running." 1>&2
  exit 1
else
  for PID in $JavaPIDS; do
    echo "JProcessID killed :" $PID
    kill -9 $PID
  done
fi
