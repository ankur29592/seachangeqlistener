package com.alticeusa.seachangelistener.jms;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.seachangelistener.util.Constants;

public class SeachangeMsgPublisher {
	private static final Logger logger = LogManager.getLogger(SeachangeMsgPublisher.class);
	
	public static SeachangeMsgPublisher instance = null;
	

	public static SeachangeMsgPublisher getInstance() {
		if (instance == null) {
			instance = new SeachangeMsgPublisher();
		} // if
		return instance;
	}

	private Connection connection = null;
	private ConnectionFactory factory = null;
	private Destination destination = null;
	private Session session = null;
	private MessageProducer producer = null;

	public void contextDestroyed() {
		try {
			if (producer != null) {
				producer.close();
			}
			if (session != null) {
				session.close();
			}
			if (connection != null) {
				connection.close();
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

	public void contextInitialized() {
		try {
			//factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
			factory = new ActiveMQConnectionFactory(Constants.SEACHANGE_BROKER_URL);
			connection = factory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(Constants.SEACHANGE_QUEUE);
			producer = session.createProducer(destination);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public void sendMessage(String msg) throws JMSException {
		try {
			contextInitialized();
			TextMessage message = session.createTextMessage();
			message.setText(msg);
			logger.info("Sending message to Queue: "+Constants.SEACHANGE_QUEUE+", Message: " + ((TextMessage) message).getText());
			producer.send(message);
			//SeachangeMsgPublisher sender = new SeachangeMsgPublisher();
			logger.info("Message Sent to Queue: "+Constants.SEACHANGE_QUEUE+", Message: " + ((TextMessage) message).getText());
			//sender.sendMessage();
		} catch (JMSException e) {
			throw e;
		} finally {
			contextDestroyed();
		}
		//throw new JMSException("Sample 'JMSException' for testing!!");
	}

	public static void main(String arg[]) {
		SeachangeMsgPublisher sender = new SeachangeMsgPublisher();
		try {
			sender.sendMessage("test message");
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}

}
