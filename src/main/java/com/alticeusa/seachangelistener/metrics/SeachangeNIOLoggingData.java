package com.alticeusa.seachangelistener.metrics;

import java.util.LinkedHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.seachangelistener.util.Constants;
import com.alticeusa.seachangelistener.util.Utils;

public class SeachangeNIOLoggingData {
	private static final Logger logger = LogManager.getLogger(SeachangeNIOLoggingData.class);
	
	private final String request;
	private final String response;
	private final  String dateTimeStamp;
	
	public String getRequest() {
		return request;
	}

	public String getResponse() {
		return response;
	}
	
	public SeachangeNIOLoggingData(String request, String response) {
		this.request = request;
		this.response = response;
		this.dateTimeStamp= Utils.getCurrentTimeStamp(Constants.METRICS_LOGGING_COMMON_DATE_TIME_FORMAT);

	}
	
	
	@Override
	public String toString() {
		return getLogMetricsJSONString();
	}
	
	
	private String getLogMetricsJSONString() {
		String metricsJSON = Constants.EMPTY_JSON;
		try {
			LinkedHashMap<String, Object> metricsKeyVaulePairs = new LinkedHashMap<String, Object>();
			addHeaderKeyValuePairs(metricsKeyVaulePairs);
			addRequestKeyValuePairs(metricsKeyVaulePairs);
			metricsJSON = MetricsLoggingUtils.buildJSONStringFromMap(metricsKeyVaulePairs);
			logger.info(metricsJSON);
			
			metricsKeyVaulePairs = new LinkedHashMap<String, Object>();
			addHeaderKeyValuePairs(metricsKeyVaulePairs);
			addResponseKeyValuePairs(metricsKeyVaulePairs);
			metricsJSON = MetricsLoggingUtils.buildJSONStringFromMap(metricsKeyVaulePairs);
			logger.info(metricsJSON);
		} catch (Exception e) {
			logger.error("Exception in SetMDRepeaterOnOff Metrics logging:", e.getMessage());
		}
		return metricsJSON;
	}

	private void addHeaderKeyValuePairs(LinkedHashMap<String, Object> metricsKeyVaulePairs) {
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_DATE_TIME_KEY, dateTimeStamp);
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_CONTEXT_NAME_KEY, Constants.METRICS_LOGGING_COMMON_CONTEXT_NAME_VALUE);
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_SUB_CONTEXT_NAME_KEY, Constants.METRICS_LOGGING_COMMON_SUB_CONTEXT_VALUE);
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_CLASS_NAME_KEY, Constants.METRICS_LOGGING_CLASS_NAME_VALUE);
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_METHOD_NAME_KEY, Constants.METRICS_LOGGING_COMMON_METHOD_NAME_VALUE);

	}

	private void addRequestKeyValuePairs(LinkedHashMap<String, Object> metricsKeyVaulePairs) {
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_REPEATER_LOG_ENTRY_TYPE, "Request");
		metricsKeyVaulePairs.put(Constants.REQUEST,request);
	}
	
	public void addResponseKeyValuePairs(LinkedHashMap<String, Object> metricsKeyVaulePairs) {
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_REPEATER_LOG_ENTRY_TYPE, "Response");
		metricsKeyVaulePairs.put(Constants.RESPONSE, response);
	}
	
	
}
