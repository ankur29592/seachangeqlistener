package com.alticeusa.seachangelistener.server;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.alticeusa.seachangelistener.util.Constants;

public final class TestClient {
	private static final Logger logger = LogManager.getLogger(TestClient.class);
	public static void main(final String[] args) {
		final InetSocketAddress hostAddress = new InetSocketAddress(Constants.SEACHANGE_LISTENER_HOST,
				Integer.parseInt(Constants.SEACHANGE_LISTENER_PORT));
		String testMsg = "";
		if (args.length > 0) {
			testMsg = args[0];
		} else {
			String errorMsg = "Input parameter is missing";
			logger.error(errorMsg);
			throw new IllegalArgumentException(errorMsg);
		}

		String response = null;
		/* for (int i = 0; i < 5; i++) { */
		try (final SocketChannel client = SocketChannel.open(hostAddress)) {
			final ByteBuffer buffer = ByteBuffer.wrap(testMsg.getBytes());

			while (buffer.hasRemaining()) {
				client.write(buffer);
			}
			buffer.flip();
			buffer.clear();
			final int bytesRead = client.read(buffer);
			logger.info("Bytes read = " + bytesRead);
			if (bytesRead == -1) {
				client.close();
			} else {
				response = new String(buffer.array());
				logger.info("response from Listener= " + response.substring(0, bytesRead));
			}
		} catch (IOException e) {
			logger.error("IOException occurred! Exception: " + e);
		}
		/* } */
	}
}
