package com.alticeusa.seachangelistener.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import javax.jms.JMSException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.alticeusa.seachangelistener.jms.SeachangeMsgPublisher;
import com.alticeusa.seachangelistener.metrics.SeachangeNIOLoggingData;
import com.alticeusa.seachangelistener.util.Constants;
import com.alticeusa.seachangelistener.util.ValidationUtil;

/**
 * The class is a Seachange to ICOM NIO Listener. This will intercept the Event
 * messages originating from Seachange MbsICOM Plugin via NIO TCP/IP Socket.
 * This class will ingest these event messages and add them to JMS Queue to
 * persist in DB.
 * 
 * @author Virendra Gulhane
 * @version 1.0
 * @since 2018-02-02
 */
public class SeachangeListener {
	private static final Logger logger = LogManager.getLogger(SeachangeListener.class);

	public static void main(String[] args) {
		String command = "";
		if (args.length > 0) {
			command = args[0];
		} else {
			String errorMsg = "Mandatory input parameter is missing, Can not proceed. [Valid input is either 'start' or 'stop']";
			logger.error(errorMsg);
			throw new IllegalArgumentException(errorMsg);
		}

		if (isValidInput(command)) {
			try (final Selector selector = Selector.open();
					final ServerSocketChannel serverSocket = ServerSocketChannel.open();) {
				final InetSocketAddress hostAddress = new InetSocketAddress(Constants.SEACHANGE_LISTENER_HOST,
						Integer.parseInt(Constants.SEACHANGE_LISTENER_PORT));
				serverSocket.bind(hostAddress);
				serverSocket.configureBlocking(false);
				serverSocket.register(selector, serverSocket.validOps(), null);
				logger.info("SeachangeListener is Running...");
				logger.info("Listening On: " + hostAddress);
				while (!"stop".equalsIgnoreCase(command) && "start".equalsIgnoreCase(command)) {
					final int numSelectedKeys = selector.select();
					if (numSelectedKeys > 0) {
						try {
						handleSelectionKeys(selector.selectedKeys(), serverSocket);
						}catch (IOException e) {
							logger.debug("IOException occurred! Exception: ", e);
						}
					}
				}

			} catch (IOException e) {
				logger.debug("IOException occurred! Exception: ", e);
			}
		} else {
			String errorMsg = "Invalid input parameter, Can not proceed. [Valid input is either 'start' or 'stop']";
			logger.error(errorMsg);
			throw new IllegalArgumentException(errorMsg);
		}
		logger.info("SeachangeListener is Shutting Down...");
	}

	public static boolean isValidInput(String inputCommand) {
		boolean input = false;
		if (inputCommand != null && !" ".equals(inputCommand)) {
			if ("start".equalsIgnoreCase(inputCommand) || "stop".equalsIgnoreCase(inputCommand)) {
				input = true;
			}
		}
		return input;
	}

	private static void handleSelectionKeys(final Set<SelectionKey> selectionKeys,
			final ServerSocketChannel serverSocket) throws IOException {
		final String METHOD_NAME = "handleSelectionKeys";
		assert !Objects.isNull(selectionKeys) && !Objects.isNull(serverSocket);

		final Iterator<SelectionKey> selectionKeyIterator = selectionKeys.iterator();
		while (selectionKeyIterator.hasNext()) {
			final SelectionKey key = selectionKeyIterator.next();

			if (key.isAcceptable()) {
				acceptClientSocket(key, serverSocket);
			} else if (key.isReadable()) {
				readRequest(key);
			} else {
				logger.info("Invalid selection key");
			}
			selectionKeyIterator.remove();
		}
	}

	private static void acceptClientSocket(final SelectionKey key, final ServerSocketChannel serverSocket)
			throws IOException {
		final String METHOD_NAME = "acceptClientSocket";
		logger.info(">>>>> " + METHOD_NAME);
		assert !Objects.isNull(key) && !Objects.isNull(serverSocket);

		final SocketChannel client = serverSocket.accept();
		client.configureBlocking(false);
		client.register(key.selector(), SelectionKey.OP_READ);

		logger.info("Accepted connection from client. Client address: " + client.getRemoteAddress());
		logger.info("<<<<< " + METHOD_NAME);
	}

	private static void readRequest(final SelectionKey key) throws IOException {
		final String METHOD_NAME = "readRequest";
		logger.info(">>>>> " + METHOD_NAME);
		assert !Objects.isNull(key);
		SeachangeMsgPublisher msgPublisher = SeachangeMsgPublisher.getInstance();
		final SocketChannel client = (SocketChannel) key.channel();
		final ByteBuffer buffer = ByteBuffer.allocate(Constants.CLIENT_BYTE_BUFFER_CAPACITY);

		final int bytesRead = client.read(buffer);

		if (bytesRead == -1) {
			client.close();
		} else {
			String msg = new String(buffer.array());
			logger.info("Received new request");
			logger.info(">>> SeachangeNIOListener :: request={} ", msg);
			long start = System.currentTimeMillis();
			// Retrieving IR No from received msg
			String IRNo = getRefNo(msg);
			//parsing message
			HashMap<String, String> hmParsedMessage = (HashMap<String, String>) ValidationUtil.parseMessage(msg);
			
			//ThreadContext
			ThreadContext.put("IR", hmParsedMessage.get("IR"));
			ThreadContext.put("corp", hmParsedMessage.get("SI"));
			
			if(hmParsedMessage.get("EA")!=null)
				ThreadContext.put("equipmentAddress", hmParsedMessage.get("EA"));
			else
				ThreadContext.put("equipmentAddress", "");
			
			if(hmParsedMessage.get("S#")!=null)
				ThreadContext.put("serialNumber", hmParsedMessage.get("S#"));
			else
				ThreadContext.put("serialNumber", "");
			
			// Validating received message
			String responseCode = ValidationUtil.validateMessage(msg);
			if (Constants.SUCCESS_RESPONSE_CODE.equals(responseCode)) {
				logger.info("Message is validated! Now inserting into Queue");
				// Sending received msg to Queue
				try {
					msgPublisher.sendMessage(msg);
					// preparing acknowledgement response for Seachange
					String ackResponse = prepareAckResponse(IRNo, responseCode);
					// Sending ACK to Seachange
					sendAcknowledgement(client, buffer, ackResponse);
					// metrics logging
					performNecessaryLogging(msg, ackResponse, start);

				} catch (JMSException e) {
					logger.warn("JMSException occurred during Sending msg to Queue. Exception: ", e);
					logger.warn("NOT sending acknowledgement to Seachange!");
					logger.warn("Event Message :" + msg);
					performNecessaryLogging(msg, IRNo, start);
					// logger.warn("Dumping this message into file now!");
					// SeachangeFileUtil.dumpRecordToFile(msg);
				} catch (Exception e) {
					performNecessaryLogging(msg, IRNo, start);
					logger.warn("Exception occurred during Sending msg to Queue! Exception :", e);

				}
			} else if (!"".equals(IRNo)) {
				logger.warn("Message is NOT validated!");
				logger.warn("Sending ACK with error response code");
				// preparing acknowledgement response for Seachange
				String ackResponse = prepareAckResponse(IRNo, responseCode);
				// Sending ACK to Seachange
				sendAcknowledgement(client, buffer, ackResponse);
				performNecessaryLogging(msg, ackResponse, start);
			} else {
				logger.warn("IR is not found in message! Can not send any ACK to Seachange!");
				performNecessaryLogging(msg, "", start);
			}
		}
		logger.info("<<<<< " + METHOD_NAME);
		ThreadContext.clearMap();
	}

	public static String getRefNo(String msg) {
		final String METHOD_NAME = "getRefNo";
		logger.info(">>>>> " + METHOD_NAME);
		String IRNo = "";
		if (!ValidationUtil.nullOrBlank(msg)) {
			int index = msg.indexOf("IR:");
			IRNo = msg.substring(index, msg.indexOf(",", index));
			logger.debug("IR Ref No= " + IRNo.trim());
		} else {
			logger.info(("Msg is null or empty"));
		}
		logger.info("<<<<< " + METHOD_NAME);
		return IRNo.trim();
	}

	public static String prepareAckResponse(String IRNo, String response) {
		final String METHOD_NAME = "prepareAckResponse";
		logger.info(">>>>> " + METHOD_NAME);
		String ackResponse = "";
		String ITTType = "I001ITT";
		String responseCode = response;
		String IRNumber = IRNo;
		String ANnumber = "AN:000000000";
		if (IRNo != null && !"".equals(IRNo.trim())) {
			ackResponse = ITTType + "," + responseCode + "," + IRNumber.trim() + "," + ANnumber + ".\\r";
			logger.debug("Acknowledgement Response: " + ackResponse);

		} else {
			logger.info(("IRNo is null or empty"));
		}
		logger.info("<<<<< " + METHOD_NAME);
		return ackResponse;
	}

	private static void sendAcknowledgement(SocketChannel client, ByteBuffer buffer, String ackResponse)
			throws IOException {
		logger.info("Sending ACK to Seachange: " + ackResponse);
		int numBytesWritten = 0;
		numBytesWritten = client.write(buffer.wrap(ackResponse.getBytes(), 00, ackResponse.length()));
		logger.info("No. of bytes sent to Seachange: " + numBytesWritten);
	}

	public static void dumpRecordToFile(String record) {
		final String METHOD_NAME = "dumpRecordToFile";
		logger.info(">>>>> " + METHOD_NAME);
		String dirName = "SeachangeFiles";
		String fileName = "Seachange_Records";

		logger.info("<<<<< " + METHOD_NAME);
	}

	public static void performNecessaryLogging(String request, String response, long startTime) {
		long endTime = System.currentTimeMillis();
		performApplicationLogging(request, response, startTime, endTime);
		performMetricsLogging(request, response);
	}

	private static void performApplicationLogging(String request, String response, long startTime, long endTime) {
		long elapsed = endTime - startTime;
		logger.info("<<< SeachangeNIOListener() [ Time taken = {} milli-seconds] ::: response = {}", elapsed,
				response);

	}

	private static void performMetricsLogging(String request, String response) {
		SeachangeNIOLoggingData metricsData = new SeachangeNIOLoggingData(request, response);
		metricsData.toString();
	}
}
