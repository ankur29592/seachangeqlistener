package com.alticeusa.seachangelistener.util;

import java.util.regex.Pattern;

/**
 * Constant class for SeachangeNIOListener.
 * @author Virendra Gulhane
 * @version 1.0
 * @since 2018-02-02
 */
public final class Constants {
	private static PropertiesReader prop = PropertiesReader.getInstance();
	public static final String SEACHANGE_QUEUE = prop.getValue("SEACHANGE_JMS_QUEUE");
	public static final String SEACHANGE_BROKER_URL = prop.getValue("SEACHANGE_BROKER_URL");
	public static final String SEACHANGE_LISTENER_HOST = prop.getValue("SEACHANGE_LISTENER_HOST");
	public static final String SEACHANGE_LISTENER_PORT = prop.getValue("SEACHANGE_LISTENER_PORT");
	public static final int CLIENT_BYTE_BUFFER_CAPACITY = Integer.parseInt(prop.getValue("CLIENT_BYTE_BUFFER_CAPACITY"));
	public static final String EXPECTED_EXCEPTION_CONNECTION_ABORTED = prop.getValue("EXPECTED_EXCEPTION_CONNECTION_ABORTED");
	public static final int ERROR_RESPONSE_CODE =111;
	public static final String ERROR_RESPONSE_MESSAGE ="";
	public static final String SUCCESS_RESPONSE_CODE ="000";
	public static final String INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE ="907";
	public static final String  MISSING_IR_RESPONSE_CODE ="555";
	public static final String INVALID_IR_RESPONSE_CODE ="555";
	public static final String MISSING_SI_RESPONSE_CODE ="555";
	public static final String INVALID_SI_RESPONSE_CODE ="555";
	public static final String MISSING_EA_RESPONSE_CODE ="321";
	public static final String INVALID_EA_RESPONSE_CODE ="321";
	public static final String MISSING_S_HASH_RESPONSE_CODE ="431";
	public static final String INVALID_S_HASH_RESPONSE_CODE ="431";
	public static final String MISSING_I$_RESPONSE_CODE ="555";
	public static final String INVALID_I$_RESPONSE_CODE ="555";
	public static final String MISSING_DT_RESPONSE_CODE ="414";
	public static final String INVALID_DT_RESPONSE_CODE ="414";
	public static final String MISSING_TM_RESPONSE_CODE ="415";
	public static final String INVALID_TM_RESPONSE_CODE ="415";
	public static final String MISSING_ID_RESPONSE_CODE ="555";
	public static final String INVALID_ID_RESPONSE_CODE ="555";
	
	// Metrics Logging
	public static final String METRICS_LOGGING_COMMON_DATE_TIME_FORMAT = "YYYY-MM-dd'T'HH:mm:ss.SSS";
	public static final String EMPTY_JSON= "{}";
	public static final String METRICS_LOGGING_COMMON_DATE_TIME_KEY= "Datetime";
	public static final String METRICS_LOGGING_COMMON_CURRENT_THREAD_KEY = "Thread";
	public static final String METRICS_LOGGING_COMMON_CONTEXT_NAME_KEY = "contextName";
	public static final String METRICS_LOGGING_COMMON_SUB_CONTEXT_NAME_KEY = "contextName";
	public static final String METRICS_LOGGING_REPEATER_LOG_ENTRY_TYPE = "LogEntryType";
	public static final String METRICS_LOGGING_COMMON_CLASS_NAME_KEY = "ClassName";
	public static final String METRICS_LOGGING_COMMON_CLASS_NAME_VALUE = "";
	public static final String METRICS_LOGGING_CLASS_NAME_VALUE = "com.alticeusa.seachangelistener.server.SeachangeListener";
	public static final String METRICS_LOGGING_COMMON_METHOD_NAME_KEY = "MethodName";
	public static final String METRICS_LOGGING_METHOD_NAME_VALUE = "handleSelectionKeys";
	public static final String METRICS_LOGGING_COMMON_CONTEXT_NAME_VALUE ="VODSeachange";
	public static final String METRICS_LOGGING_COMMON_SUB_CONTEXT_VALUE = "VODSeachangeNIOListener";
	public static final String METRICS_LOGGING_COMMON_METHOD_NAME_VALUE = "readRequest";
	public static final String METRICS_LOGGING_COMMON_ERROR_CODE="ErrorCode";
	public static final String METRICS_LOGGING_COMMON_ERROR_MESSAGE= "ErrorMessage";
	public static final String METRICS_LOGGING_COMMON_STATE="State";
	public static final String RESPONSE = "Response";
	public static final String REQUEST = "Request";
	public static final Pattern maskRegex = Pattern.compile("\\b[0-9]{9}\\b|\\b[0-9]{16}\\b|\\b[0-9]{3}-[0-9]{2}-[0-9]{4}\\b");
	public static final String SSN_MASK="XXXXXXXXX";
	
}
