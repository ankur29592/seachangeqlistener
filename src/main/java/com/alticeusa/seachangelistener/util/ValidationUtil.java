package com.alticeusa.seachangelistener.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.seachangelistener.server.SeachangeListener;

/**
 * This class provides validation utility method to validate messages received
 * from Seachange.
 * 
 * @author Virendra Gulhane
 * @version 1.0
 * @since 2018-02-19
 */
public class ValidationUtil {
	private static final Logger logger = LogManager.getLogger(SeachangeListener.class);

	public static String validateMessage(String message) {
		final String METHOD_NAME = "validateMessage";
		logger.info(">>>>> " + METHOD_NAME);
		String responseCode = Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE;
		StringBuilder validationMessage = new StringBuilder();
		if (message != null && !"".equals(message.trim())) {
			boolean ir_flag = false;
			boolean eaORs_Flag = false;
			boolean dt_flag = false;
			boolean tm_flag = false;
			boolean i$_flag = false;
			boolean id_flag = false;
			boolean si_flag = false;

			// parsing message into key value pair
			HashMap<String, String> hmParsedMessage = (HashMap<String, String>) parseMessage(message);

			// validating IR
			if (hmParsedMessage.get("IR") != null) {
				ir_flag = validateIR(hmParsedMessage.get("IR"));
				if (!ir_flag) {
					logger.debug("IR is invalid in message");
					validationMessage.append("IR is invalid in message");
					responseCode = Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE;
					logger.info("<<<<< " + METHOD_NAME);
					return responseCode;
				}
			} else {
				ir_flag = false;
				logger.debug("IR is missing in message");
				validationMessage.append("IR is missing in message");
				responseCode = Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE;
				logger.info("<<<<< " + METHOD_NAME);
				return responseCode;
			}

			// validating SI
			if (hmParsedMessage.get("SI") != null) {
				si_flag = validateSI(hmParsedMessage.get("SI"));
				if (!si_flag) {
					logger.debug("SI is invalid in message");
					validationMessage.append("SI is invalid in message");
					responseCode = Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE;
					logger.info("<<<<< " + METHOD_NAME);
					return responseCode;
				}
			} else {
				si_flag = false;
				logger.debug("SI is missing in message");
				validationMessage.append("SI is missing in message");
				responseCode = Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE;
				logger.info("<<<<< " + METHOD_NAME);
				return responseCode;
			}

			// validating EA or S#
			if (hmParsedMessage.get("EA") != null) {
				eaORs_Flag = validateEAorS(hmParsedMessage.get("EA"), "EA");
				if (!eaORs_Flag) {
					logger.debug("EA is invalid in message");
					validationMessage.append("EA is invalid in message");
					responseCode = Constants.INVALID_EA_RESPONSE_CODE;
					logger.info("<<<<< " + METHOD_NAME);
					return responseCode;
				}
			} else if (hmParsedMessage.get("S#") != null) {
				eaORs_Flag = validateEAorS(hmParsedMessage.get("S#"), "S#");
				if (!eaORs_Flag) {
					logger.debug("S# is invalid in message");
					validationMessage.append("S# is invalid in message");
					responseCode = Constants.INVALID_S_HASH_RESPONSE_CODE;
					logger.info("<<<<< " + METHOD_NAME);
					return responseCode;
				}
			} else {
				eaORs_Flag = false;
				logger.debug("mandatory field EA and S# is missing in message");
				validationMessage.append("mandatory field EA and S# is missing in message");
				responseCode = Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE;
				logger.info("<<<<< " + METHOD_NAME);
				return responseCode;
			}

			// Validating I$
			if (hmParsedMessage.get("I$") != null) {
				i$_flag = validateI_Dollar(hmParsedMessage.get("I$"));
				if (!i$_flag) {
					logger.debug("I$ is invalid in message");
					validationMessage.append("I$ is invalid in message");
					responseCode = Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE;
					logger.info("<<<<< " + METHOD_NAME);
					return responseCode;
				}
			} else {
				i$_flag = false;
				logger.debug("I$ is missing in message");
				validationMessage.append("I$ is missing in message");
				responseCode = Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE;
				logger.info("<<<<< " + METHOD_NAME);
				return responseCode;
			}

			// validating DT
			if (hmParsedMessage.get("DT") != null) {
				logger.debug("input date :" + hmParsedMessage.get("DT"));
				// Removing extra identifies from input date
				String date = hmParsedMessage.get("DT").substring(1);
				logger.debug("extracted date :" + date);
				dt_flag = validateDT(date);
				if (!dt_flag) {
					logger.debug("DT is invalid in message");
					validationMessage.append("DT is invalid in message");
					responseCode = Constants.INVALID_DT_RESPONSE_CODE;
					logger.info("<<<<< " + METHOD_NAME);
					return responseCode;
				}
			} else {
				dt_flag = false;
				logger.debug("DT is missing in message");
				validationMessage.append("DT is missing in message");
				responseCode = Constants.MISSING_DT_RESPONSE_CODE;
				logger.info("<<<<< " + METHOD_NAME);
				return responseCode;
			}

			// validating TM
			if (hmParsedMessage.get("TM") != null) {
				tm_flag = validateTM(hmParsedMessage.get("TM"));
				if (!tm_flag) {
					logger.debug("TM is invalid in message");
					validationMessage.append("TM is invalid in message");
					responseCode = Constants.INVALID_TM_RESPONSE_CODE;
					logger.info("<<<<< " + METHOD_NAME);
					return responseCode;
				}
			} else {
				tm_flag = false;
				logger.debug("TM is missing in message");
				validationMessage.append("TM is missing in message");
				responseCode = Constants.MISSING_TM_RESPONSE_CODE;
				logger.info("<<<<< " + METHOD_NAME);
				return responseCode;
			}

			// validating ID
			if (hmParsedMessage.get("ID") != null) {
				id_flag = validateID(hmParsedMessage.get("ID"));
				if (!id_flag) {
					logger.debug("ID is invalid in message");
					validationMessage.append("ID is invalid in message");
					responseCode = Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE;
					logger.info("<<<<< " + METHOD_NAME);
					return responseCode;
				}
			} else {
				id_flag = false;
				logger.debug("ID is missing in message");
				validationMessage.append("ID is missing in message");
				responseCode = Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE;
				logger.info("<<<<< " + METHOD_NAME);
				return responseCode;
			}

			String msg = "all validations are Successfull for message: " + message;
			logger.info(msg);
			logger.info("Response code: " + Constants.SUCCESS_RESPONSE_CODE);
			validationMessage.append(msg);
			responseCode = Constants.SUCCESS_RESPONSE_CODE;

		} else {
			logger.warn("Message received from Seachange is null!");
			responseCode = Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE;
			logger.info("<<<<< " + METHOD_NAME);
			return responseCode;
		}
		logger.info("Validation Result: " + validationMessage);
		logger.info("<<<<< " + METHOD_NAME);
		return responseCode;
	}

	public static Map<String, String> parseMessage(String message) {
		Map<String, String> hmParsedMsg = new HashMap<>();
		StringTokenizer st = new StringTokenizer(message, ",");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (token.contains(":")) {
				String key = token.substring(0, token.indexOf(":"));
				String val = token.substring(token.indexOf(":") + 1);
				hmParsedMsg.put(key.trim(), val.trim());
			}
		}
		return hmParsedMsg;
	}

	public static boolean nullOrBlank(String inputString) {
		return (inputString == null || (inputString.trim().equalsIgnoreCase("")) || (inputString.trim().length() == 0)
				|| (inputString.trim().equalsIgnoreCase("null")));
	}

	public static boolean isAllNumeric(String value) {
		if (value == null) {
			return false;
		}
		if (value.matches("[0-9]+")) {
			return true;
		}
		logger.debug("value is NOT Numeric!: " + value);
		return false;
	}

	public static boolean validateIR(String irNumber) {
		if (!nullOrBlank(irNumber)) {
			logger.debug("IR validated! IR: " + irNumber);
			return true;
		}
		logger.debug("IR is NOT valid! IR: " + irNumber);
		return false;
	}

	public static boolean validateEAorS(String valueToValidate, String key) {
		if (key.equalsIgnoreCase("EA")) {
			logger.debug("Validating EA: " + valueToValidate);
			if (!nullOrBlank(valueToValidate)) {
				logger.debug("EA validated! EA: " + valueToValidate);
				return true;
			} else {
				logger.debug("EA is NOT valid! EA: " + valueToValidate);
			}
		} else if (key.equalsIgnoreCase("S#")) {
			logger.debug("Validating S#: " + valueToValidate);
			if (!nullOrBlank(valueToValidate)) {
				logger.debug("S# validated! S#: " + valueToValidate);
				return true;
			} else {
				logger.debug("S# is NOT valid! S#: " + valueToValidate);
			}
		} else {
			logger.debug("Can not validate EA or S# for key: " + key + " value: " + valueToValidate);
		}
		return false;
	}

	public static boolean validateDT(String dt) {
		if (dt == null) {
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		sdf.setLenient(false);
		try {
			// if not valid, it will throw ParseException
			Date date = sdf.parse(dt);
			logger.debug("DT is validated!");
			logger.debug("Input Date: " + dt);
			logger.debug("Parsed Date: " + date);
		} catch (ParseException e) {
			logger.debug("DT is NOT valid: " + dt);
			logger.debug("Error validating date: " + dt + " exception: "+e);
			return false;
		}
		return true;
	}

	public static boolean validateTM(String tm) {
		if (tm == null) {
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
		sdf.setLenient(false);
		try {
			// if not valid, it will throw ParseException
			Date date = sdf.parse(tm);
			logger.debug("TM is validated!");
			logger.debug("Input Time: " + tm);
			logger.debug("Parsed Time: " + sdf.format(date));
		} catch (ParseException e) {
			logger.debug("TM is NOT valid: " + tm);
			logger.debug("Error validating date: " + tm + " exception: "+e);
			return false;
		}
		return true;
	}

	public static boolean validateI_Dollar(String i_dollar) {
		if (isAllNumeric(i_dollar)) {
			logger.debug("I$ validated! I$: " + i_dollar);
			return true;
		}
		logger.debug("I$ is NOT valid! I$: " + i_dollar);
		return false;
	}

	public static boolean validateID(String id) {
		if (!nullOrBlank(id)) {
			logger.debug("ID validated! ID: " + id);
			return true;
		}
		logger.debug("ID is NOT valid! ID: " + id);
		return false;
	}

	public static boolean validateSI(String SI) {
		if (isAllNumeric(SI)) {
			logger.debug("SI validated! SI: " + SI);
			return true;
		}
		logger.debug("SI is NOT valid! SI: " + SI);
		return false;
	}
}
