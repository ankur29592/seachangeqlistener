package com.alticeusa.seachangelistener.util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SeachangeFileUtil {
	private static final Logger logger = LogManager.getLogger(SeachangeFileUtil.class);

	public static void dumpRecordToFile(String record) {
		
		StringBuffer dirName = new StringBuffer("SeachangeFiles");
		StringBuffer fileName = new StringBuffer("seachange_records_");
		Path path = Paths.get(dirName.toString());
		// if directory exists?
		if (!Files.exists(path)) {
			try {
				logger.info("Creating directory...");
				Files.createDirectories(path);
				logger.info("Directory created...");

			} catch (IOException e) {
				// fail to create directory
				logger.error("IOException occurred during creating directory. Exception: "+e);
			}
		}
		// logger.info("Directory Exists");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		String dateStr = dateFormat.format(cal.getTime());
		fileName.append(dateStr);
		fileName.append(".txt");
		StringBuffer filePath = dirName.append("\\");
		filePath.append(fileName);
		logger.info("File Path: " + filePath);
		logger.info("File Name: " + fileName);
		if (!Files.exists(Paths.get(filePath.toString()), LinkOption.NOFOLLOW_LINKS)) {
			logger.info("Creating File...");
			try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(filePath.toString()),
					Charset.forName("UTF-8"))) {
				writer.write(record);
				logger.info("File created...");	
			} catch (IOException ex) {
				logger.error("IOException occurred during creating file. Exception: "+ex);
			}

		} else {
			logger.info("File: "+fileName+ " exists! appending records to file");
			String lineSeparator = System.lineSeparator();
			try {
				Files.write(Paths.get(filePath.toString()), lineSeparator.getBytes(), StandardOpenOption.APPEND);
				Files.write(Paths.get(filePath.toString()), lineSeparator.getBytes(), StandardOpenOption.APPEND);
				Files.write(Paths.get(filePath.toString()), record.getBytes(), StandardOpenOption.APPEND);
			} catch (IOException e) {
				logger.error("IOException occurred during appending records to file. Exception: "+e);
			}

		}

	}
}
