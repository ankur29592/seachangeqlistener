package com.alticeusa.seachangelistener.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;


/**
 * This is utility class
 * 
 * @author Dhirenda Gautam
 * @version 1.0
 * @since 2017-10-27
 */

public class Utils {

	private static final Logger logger = LogManager.getLogger(Utils.class);

	public static String convertJSONObjectToString(Object obj)
			throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper objMapper = new ObjectMapper();
		String json = objMapper.writeValueAsString(obj);
		return json;
	}

	public static String getCurrentTimeStamp(String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		Date now = new Date();
		String formattedDateTime = dateFormat.format(now);
		return formattedDateTime;
	}


	public static void populateResponseWithValuesFromRequest(String request,
			String response) {
	}


	public static void performApplicationLogging(String request, String response,
			long startTime, long endTime) {
		long elapsed = endTime - startTime;
		logger.info(" SeachangeNIOListener() [ Time Taken = { } milli-seconds] ::: response = {}", elapsed, response);
	}

}
