package com.alticeusa.seachangelistener.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.alticeusa.seachangelistener.server.SeachangeListener;

public class SeachangeListenerTest {
	
	static String refNo="";
	static String refInputMsg="";
	static String ackResponse="";
	static String successResponseCode;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		successResponseCode="000";
		refNo="IR:284446598";
		refInputMsg="A001ITT,IR:284446598                       ,SI:811,EA:0000083849658101                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		ackResponse="I001ITT,000,IR:284446598,AN:000000000.\\r";
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testIsValidInputCase1() {
		assertTrue(SeachangeListener.isValidInput("start"));		
	}
	
	@Test
	public void testIsValidInputCase2() {
		assertTrue(SeachangeListener.isValidInput("stop"));		
	}
	
	@Test
	public void testIsValidInputCase3() {
		assertFalse(SeachangeListener.isValidInput("abcd"));		
	}
	
	@Test
	public void testGetRefNoCase1() {
		assertEquals(refNo, SeachangeListener.getRefNo(refInputMsg));
	}
	
	@Test
	public void testGetRefNoCase2() {
		assertEquals("", SeachangeListener.getRefNo(null));
	}
	
	@Test
	public void testPrepareAckResponseCase1() {
		assertEquals(ackResponse, SeachangeListener.prepareAckResponse(refNo,successResponseCode));
	}
	
	@Test
	public void testPrepareAckResponseCase2() {
		assertEquals("", SeachangeListener.prepareAckResponse(null,successResponseCode));
	}
	
	
}
