package com.alticeusa.seachangelistener.test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.alticeusa.seachangelistener.util.Constants;
import com.alticeusa.seachangelistener.util.ValidationUtil;

public class ValidationUtilTest {
	static String refNo="";
	static String refInputMsg="";
	static String ackResponse="";
	static int successResponseCode;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		successResponseCode=000;
		refNo="IR:284446598";
		refInputMsg="A001ITT,IR:284446598                       ,SI:811,EA:0000083849658101                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		ackResponse="I000ITT,000,IR:284446598,AN:000000000.\\r";
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	//@Ignore
	@Test
	public void validateMessageSuccess() {
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.SUCCESS_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void validateMessageSuccess_S_Hash() {
		String refInputMsgWithS_Hash="A001ITT,IR:284446598                       ,SI:811,S#:0123456789                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsgWithS_Hash)==Constants.SUCCESS_RESPONSE_CODE);
	}
	
	//@Ignore
	@Test
	public void validateMessageInvalidIR() {
		String refInputMsg="A001ITT,IR:                      ,SI:811,EA:0000083849658101                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void validateMessageMissingIR() {
		String refInputMsg="A001ITT,SI:811,EA:0000083849658101                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void invalidSI() {
		String refInputMsg="A001ITT,IR:284446598                       ,SI:811xyz,EA:0000083849658101                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void missingSI() {
		String refInputMsg="A001ITT,IR:284446598                       ,EA:0000083849658101                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void invalidEA() {
		String refInputMsg="A001ITT,IR:284446598                       ,SI:811,EA:                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_EA_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void invalidS_Hash() {
		String refInputMsg="A001ITT,IR:284446598                       ,SI:811,S#:                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_S_HASH_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void missingEAorS_Hash() {
		String refInputMsg="A001ITT,IR:284446598                       ,SI:811,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void invalidI$() {
		String refInputMsg="A001ITT,IR:284446598                       ,SI:811,EA:0000083849658101                ,I$:000000499xyz,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void missingI$() {
		String refInputMsg="A001ITT,IR:284446598                       ,SI:811,EA:0000083849658101                ,II:AK2PT     ,IM:000301,DT:1171027,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void invalidDT() {
		String refInputMsg="A001ITT,IR:284446598                       ,SI:811,EA:0000083849658101                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1180229,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_DT_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void missingDT() {
		String refInputMsg="A001ITT,IR:284446598                       ,SI:811,EA:0000083849658101                ,I$:000000499,II:AK2PT     ,IM:000301,TM:135856,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_DT_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void invalidTM() {
		String refInputMsg="A001ITT,IR:284446598                       ,SI:811,EA:0000083849658101                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027, TM:135860,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_TM_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void missingTM() {
		String refInputMsg="A001ITT,IR:284446598                       ,SI:811,EA:0000083849658101                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,ID:Padmavat: Home                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_TM_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void invalidID() {
		String refInputMsg="A001ITT,IR:284446598                       ,SI:811,EA:0000083849658101                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027, TM:135859       ,ID:                   ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE));
	}
	
	//@Ignore
	@Test
	public void missingID() {
		String refInputMsg="A001ITT,IR:284446598                       ,SI:811,EA:0000083849658101                ,I$:000000499,II:AK2PT     ,IM:000301,DT:1171027,TM:135959              ,IT:Y,IU:Y,IS:000,IC:699       ,IV:400,IL:1,UP";
		assertTrue(ValidationUtil.validateMessage(refInputMsg).equals(Constants.INVALID_VALUE_FOR_TOKEN_RESPONSE_CODE));
	}
	
	
}
